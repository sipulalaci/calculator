[![Generic badge](https://img.shields.io/badge/interview-calculator-blueviolet.svg)](https://shields.io/)
# Calculator

 Just a simple calculator application with standard operations like addition, subtraction, multiplication and division with Angular frontend and Express backend.

 ![](documents/calculator.png)

## Getting started

### Prerequisites

- [Node.js](https://nodejs.org/en/) for both backend and frontend
- [Angular CLI](https://angular.io/start/start-deployment) for the `ng` commands
- [ExpressJS](https://expressjs.com/) for the backend service

### Installing Node.js
We can download Node.js from the link provided above. After downloading we can install it with the help of installation wizard.
After installing Node.js we can start to take care of the backend and frontend.

### Running the backend

To be able to run our backend service, we have to install the express framework inside the backend folder.
```
cd backend
npm install --save express
 ```
After express was installed, we can start the backend service with `node memory.js` or `npm start` command.

If the backend service started successfully, we have to see the `Backend service is listening on port 3000` in the console and we can reach it at `http://localhost:3000`

### Running the frontend

After installing Node.js we can start installing the angular command line interface (Angular CLI) from npm.
- run the command `npm install -g @angular/cli`
- after the angular CLI was successfully installed, we can start the application with the command: `ng serve` or `npm start`
- and after a few seconds the application is going to be available on [`http://localhost:4200`](http://localhost:4200)



## How does it work
On top of the calculator is the display, where two line are to found. In the first line we have the current operation with the two operands and the operator. In the second line we have the result of the operation after clicking on the equal button.
On the bottom part of the application we have the controls. The control keys can be grouped as Memory keys, Operation keys, Number keys and the Other keys (Equal button, Decimal point button and Clear button).

### Memory
After we successfully calculated an operation by clicking on the equal button, we can save the result of it for subsequent use. This can be done with the MS button, which refers to Memory Save.
There is the MR button, like Memory Recall. We can recall a value to the left and right operands as well. 
After we clicked on the MR button, the value will be loaded based on, if we have an operator or not. After the value was loaded, we can add more digits or a decimal part to it.
If there is a result on the screen and we click on the MR button, the saved value will be loaded as the left operand and all the other elements will be cleared.

### Operations
By default, when there is no left operand nothing happens if we click on any of the operation buttons. If we add a number as the left operand and we click on any of them, the value of the button (+, -, * or /) will be added to the screen after the left operand. If we add the other number as the right operand and we click on any of the operation keys, the operation will be calculated, the result loaded as the left operand and the operation sign as the operator.
Dividing by 0 gives an error.

### Numbers
By clicking on the numbers, the number selected will be appended to the left or right operand based on if we have an operator already set.
If there is a result calculated and we click on any of the number keys, the display will be cleared and a new operation is started.

### Other keys
#### Decimal point
It works almost like the number keys. If there is no operand and we click on the button, the value of "0." will be added as the left operand, so it's faster than clicking on the key of 0 and then the decimal point button. If there are already digits of a number, then the decimal point is appended to the number. If the number already includes a decimal point, no more is added to it.
If the left operand and the operator is already added, then the same happens to the right operand.

#### Clear button
All the values on the display are cleared after clicking on it.

#### Equal button
When both of the operands are added, it means that the operator is added too, so when clicking on the button, the operation is calculated.
If there is only the left operand or the left operand and the operator and we click on the equal button the left operand is added as the result. It's easier to save a number to the memory, than typing "value + 0" and the equal button to save it.

## Learn More

To learn more about the technologies, take a look at the following resources:

- [Angular](https://angular.io/) - learn about Angular
- [Express](https://expressjs.com/) - introduction to Express

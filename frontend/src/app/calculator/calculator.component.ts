import { Component, OnInit } from '@angular/core';
import { MemoryService } from '../_services/memory.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent implements OnInit {
  public currentCalculation: string = '';
  public result: string = '';

  leftOperand: string = '';
  rightOperand: string = '';
  operator: string = '';

  constructor(private memoryService: MemoryService) {}

  ngOnInit() {}

  onClear() {
    this.leftOperand =
      this.rightOperand =
      this.operator =
      this.currentCalculation =
      this.result =
        '';
  }

  onNumberClick(event) {
    if (this.isResultCalculated()) {
      this.onClear();
    }
    if (this.operator === '') {
      this.leftOperand += event.target.value;
    } else {
      this.rightOperand += event.target.value;
    }
  }

  onDecimalClick() {
    if (this.isResultCalculated()) {
      this.onClear();
    }

    if (this.operator === '') {
      this.decimalInLeftOperand();
    } else {
      this.decimalInRightOperand();
    }
  }

  decimalInLeftOperand() {
    if (this.leftOperand.includes('.')) {
        return;
      } else if (this.leftOperand.length === 0) {
        this.leftOperand += '0.';
      } else {
        this.leftOperand += '.';
      }
  }

  decimalInRightOperand() {
    if (this.rightOperand.includes('.')) {
        return;
      } else if (this.rightOperand.length === 0) {
        this.rightOperand += '0.';
      } else {
        this.rightOperand += '.';
      }
  }

  onOperatorClick(event) {
    // If there is a result and the user clicks on any of the operators, we can assume, that the user wants to work with the result
    if (this.isResultCalculated()) {
      this.leftOperand = this.result;
      this.rightOperand = this.result = '';
      this.operator = event.target.value;
    } else if (this.leftOperandOnly() || this.leftOperandWithOperator()) {
      this.operator = event.target.value;
    } else if (this.bothOperandsFilled()) {
      this.onCalculate();
      this.onOperatorClick(event);
    }
  }


  isResultCalculated() {
    if (this.result !== '') {
      return true;
    } else {
      return false;
    }
  }

  bothOperandsFilled() {
    if (
      this.leftOperand !== '' &&
      this.rightOperand !== '' &&
      this.operator !== ''
    ) {
      return true;
    } else {
      return false;
    }
  }

  leftOperandOnly() {
    if (this.leftOperand !== "" && this.operator === "" && this.rightOperand === "") {
      return true;
    }
    else {
      return false;
    }
  }
  
  leftOperandWithOperator() {
    if (this.leftOperand !== "" && this.operator !== "" && this.rightOperand === "") {
      return true;
    }
    else {
      return false;
    }
  }

  onCalculate() {
    if ( this.bothOperandsFilled() ) {
      const val1 = parseFloat(this.leftOperand);
      const val2 = parseFloat(this.rightOperand);
      switch (this.operator) {
        case '+': {
          this.result = (val1 + val2).toString();
          break;
        }
        case '-': {
          this.result = (val1 - val2).toString();
          break;
        }
        case '*': {
          this.result = (val1 * val2).toString();
          break;
        }
        case '/': {
          if (val2 === parseFloat("0")) {
            this.result = "Error"
          } else {
            this.result = (val1 / val2).toString();
          }
          break;
        }
        default: {
          break;
        }
      }
    }
    else if (this.leftOperandOnly()) {
      this.result = this.leftOperand;
    }
  }

  onMemorySave() {
    if (this.result !== '') {
      this.memoryService.saveValue(this.result).subscribe(red=> console.log("Value saved!"));
    }
  }

  onMemoryLoad() {
    this.memoryService.loadValue().subscribe(res => {
      const loadedValue = res.toString();
      console.log("Loaded value: ", loadedValue);
      
      if (this.leftOperandOnly()) {
        return;
      } else if (this.leftOperandWithOperator()) {
        this.rightOperand += loadedValue;
      } else if (this.isResultCalculated() || !this.bothOperandsFilled()) {
        this.onClear();
        this.leftOperand = loadedValue;
      }
    });
    
  }
}

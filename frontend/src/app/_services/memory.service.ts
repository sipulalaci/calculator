import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MemoryService {
  apiURL = environment.apiURL;

  constructor(private http: HttpClient) { }

  saveValue(value: string){
    return this.http.post(`${this.apiURL}save`, value);
  }

  loadValue(){
    return this.http.get(`${this.apiURL}load`);
  }
}

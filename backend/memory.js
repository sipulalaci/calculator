const express = require("express");
const fileSystem = require("fs");
const cors = require("cors");

const app = express();
const port = 3000;

// Filename, where we save the value
const fileName = "./saved_value.txt";

app.use(cors());

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.post("/save", (req, res) => {

  req.on('data', data => {
    const val = data.toString();
    console.log("Data to be saved:", val);

    fileSystem.writeFile(fileName, val, (err) => {
      if (err) {
        return console.log(err);
      }
      console.log("Value was saved successfully.");
    });
  });
 
  
});

app.get("/load", (req, res) => {
  //load from file
  fileSystem.readFile(fileName, "utf8", (err, data) => {
    if (err) {
      console.log(err);
      return;
    }

    console.log('Data loaded: ',data);
    res.json(data);
  });
});

app.listen(port, () =>
  console.log("Backend service listening on port " + port)
);
